const express        = require('express');
const app            = express();
const MongoClient    = require('mongodb').MongoClient;


const port = 8000;
const url = 'mongodb://localhost:27017/app';
const users = require('./users.json');
const list = users.listUsers;
// var  list = JSON.parse(users);


MongoClient.connect(url, (err, db) => {
    if (err) {
        console.log('Can\'t connect to MongoDB server. Error:', err);
    } else {
        console.log('Connection!!!');

        var collection = db.collection('users');

        for (var i=0; i < list.length; i++) {
            collection.insert(list[i], function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    console.log('ok');
                }               
            });

        };


// если нет поля gender, вставить gender: "m"
         collection.find({"gender":{"$exists":false}}).toArray(function (err, result) {
            if (err) {
                console.log(err);
            } else if (result.length) {
                console.log(result.length);
                collection.update({"gender":{"$exists":false}}, {$set: {gender: "m"}}, {multi:true});
            } else {
                console.log('Нет документов с таким условием поиска');  
            }     
         });

// заменить имена
        var namesOld = ["Александр", "Архип"];
        var namesNew = ["Alexander", "Arkhip"];

        if (namesNew.length !== namesOld.length) {
            console.log("Количество новых имен не совпадает с количеством старых");
        } else {
            namesOld.forEach(function(element, index) {
                collection.update({"name":element},{$set: {'name': namesNew[index]}}, {multi:true});
            });
        };

//отобразить список
        collection.find().toArray(function (err, result){
            if (err) {
                    console.log(err);
                } else {
                    console.log(result);
                };          
        });

//Удалить новые имена
        namesNew.forEach(function(element, index) {
            collection.remove({"name":element}, true);
        });

        db.close;
    }; 
});



app.listen(port, () => {
  console.log('We are live on ' + port);
});